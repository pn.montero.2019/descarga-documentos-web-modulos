import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.content = None

    def retrieve(self):
        if not self.content:    # si hay contenido
            with urllib.request.urlopen(self.url) as response:
                self.content = response.read().decode('utf-8')
                print("Downloading", self.url)

    def show(self):
        self.retrieve()
        print(self.content)

    def content(self):
        self.retrieve()
        return self.content
