from robot import Robot
from cache import Cache


if __name__ == '__main__':
    print("Test Cache class")
    url1 = 'http://gsyc.urjc.es/'
    c = Cache()
    c.retrieve(url1)
    c.show(url1)

    url2 = "https://www.ejemplo.com/documento2.txt"
    c.retrieve(url2)
    c.show(url2)

    print("List of url's downloaded")
    c.show_all()

    print("Test Robot class")
    url3 = "https://www.ejemplo.com/documento1.txt"
    r = Robot(url3)
    r.retrieve()
    r.show()


